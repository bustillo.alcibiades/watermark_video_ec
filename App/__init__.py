from random import randint
import numpy as np
import os
import cv2
from os.path import isfile, join
import matplotlib.pyplot as plt
from scipy import signal

from numpy import *
from mpl_toolkits.mplot3d import Axes3D



# Arreglo de Legendre utilizado en buil3DL
m7 =  np.load('legendreM7.npy')
n,m = m7.shape
mat = np.zeros((n,m), dtype=np.int8)
mat = np.int8(m7) * 40

# Variable utilizada en la funcion insert_wm_images
marca_position = {}
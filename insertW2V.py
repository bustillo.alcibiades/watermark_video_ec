
from app import *


directory = os.getcwd()
folder = "/v2i"                               # Folder to strore the video frames
path = directory + folder 

folder2 = "/i-with-w"                         # Folder to store the watermarked frames 
path2 = directory + folder2

folder3 = "/WaterImages"                      # Folder to store the video watermaked frames
path3 = directory + folder3

# Crear folders
def createFolders():
    try:  
        os.mkdir(path)
        os.mkdir(path2)
        os.mkdir(path3)
    except OSError:  
        print ("Creation of the directory %s failed" % path)
        print ("Creation of the directory %s failed" % path2)
        print ("Creation of the directory %s failed" % path3)
    else:  
        print ("Successfully created the directory %s " % path)
        print ("Successfully created the directory %s " % path2)
        print ("Successfully created the directory %s " % path3)

# Convertir video a secuencia de imagenes
def convertVideo2Images():
    #vidcap = cv2.VideoCapture('drop.avi')
    vidcap = cv2.VideoCapture('test.mp4')
    success,image = vidcap.read()
    count = 0
    success = True
    while success:
        cv2.imwrite(path + "/frame%d.jpg" % count, image)     # save frame as JPEG file
        success,image = vidcap.read()
        #print ('Read a new frame: ', success)
        count += 1

def build3DL(legendreM, s):    
    # La variable mat3D almacena la matriz 3D
    mat3D = []
    for i in range(len(s)):
        x = np.roll(legendreM, s[i][1], axis = 1)
        y = np.roll(x, s[i][0], axis = 0)
        #print y
        #print ""
        #print ""
        mat3D.append(y)
    #aux = np.zeros((p,p), dtype=np.float)    
    mat3D.append(legendreM)
    return mat3D

def insert_wm_images(pathIn,pathOut, mat3d):
    p = len(mat3d)
    n,m = mat3d[0].shape
    
    frame_array = []
    files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]
    
    #for sorting the file names properly
    files.sort(key = lambda x: int(x[5:-4]))
    
    for k in range(len(files)):
        filename=pathIn + files[k]
        #reading each files
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        
        y = randint(30,width - n-100)
        x = randint(30,height - m-50)
        
        marca_position[k] = (x,y)
        
        for i in range(n):
            for j in range(m):
                img[x+i,y+j, 0] = img[x+i,y+j,0] + mat3d[k%p][i,j]
                img[x+i,y+j, 1] = img[x+i,y+j,1] + mat3d[k%p][i,j]
                img[x+i,y+j, 2] = img[x+i,y+j,2] + mat3d[k%p][i,j]
        #print(mat3d[k%p])
                
        
        
        #inserting the frames into an image array
        frame_array.append(img)
    
     
    for i in range(len(frame_array)):
        # writing to a image array        
        cv2.imwrite(pathOut + "/watermark%d.jpg" % i, frame_array[i])

def compute_cross_correlation(mat3d):
    #b, a = marca_position[12]

    mat = mat3d[35%11]
    #print(mat)

    img2 = cv2.imread(path2+'/watermark35.jpg')
    #print (type(img2))
    red = img2[:,:,2]

    red = (red - np.mean(red)) / (np.std(red) * len(red))
    mat = (mat - np.mean(mat)) / (np.std(mat))

    corr = signal.correlate2d(red, mat, mode='same')
    y, x = np.unravel_index(np.argmax(corr), corr.shape)

    #print(corr[x,y])
    #print(x,y)
    #print(np.argmax(corr))

    fig, (ax_orig, ax_template, ax_corr, ax_corrPlot) = plt.subplots(4, 1, figsize=(30, 30))
    ax_orig.imshow(red, cmap='gray')
    ax_orig.set_title('Original')
    ax_orig.set_axis_off()
    ax_template.imshow(mat, cmap='gray')
    ax_template.set_title('Template')
    ax_template.set_axis_off()
    ax_corr.imshow(corr, cmap='gray')
    
    
        
    ax_corr.set_title('Cross-correlation')
    ax_corr.set_axis_off()
    ax_orig.plot(x, y, 'ro')
    ax_corrPlot.plot(np.absolute(corr))
    ax_corrPlot.set_title('Cross-correlation')
    #ax_corrPlot.set_axis_off()
    fig.show()
    fig.savefig('plot1.pdf')

    #corr[2,15]
    ind = np.unravel_index(np.argmax(corr, axis=None), corr.shape)
    i,j = ind
    print(ind)
    #print(corr(ind))
    return corr

def plot_cross_correlation3D(corr):
    n,m = corr.shape
    x , y = mgrid [0:n,0:m]
    f = corr
    fig = plt.figure() 
    ax = Axes3D(fig)
    ax.plot_surface(x,y,f,rstride=1, cstride=1)
    plt.show()
    fig.savefig('cross3d.pdf')

def convert_frames_to_video(pathIn,pathOut,fps):
    frame_array = []
    files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]
    
    #for sorting the file names properly
    files.sort(key = lambda x: int(x[9:-4]))
    
    for i in range(len(files)):
        filename=pathIn + files[i]
        #reading each files
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        #print(filename)
        #inserting the frames into an image array
        frame_array.append(img)
    #out = cv2.VideoWriter(pathOut,cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
    out = cv2.VideoWriter(pathOut,cv2.VideoWriter_fourcc(*'MP4V'), fps, size)
     
    for i in range(len(frame_array)):
        # writing to a image array
        out.write(frame_array[i])
    out.release()

def convert_watermark_images():
    vidcap = cv2.VideoCapture('salida.mp4')
    success,image = vidcap.read()
    count = 0
    success = True
    while success:
        cv2.imwrite(path3 + "/water%d.jpg" % count, image)     # save frame as JPEG file
        success,image = vidcap.read()
        #print ('Read a new frame: ', success)
        count += 1  

def compute_cross_correlation_watermark(mat3d):
    #b, a = marca_position[12]
    mat = mat3d[35%11]
    img2 = cv2.imread(path3+'/water35.jpg')
    #print (type(img2))
    red = img2[:,:,2]
    red = (red - np.mean(red)) / (np.std(red) * len(red))
    mat = (mat - np.mean(mat)) / (np.std(mat))

    corr = signal.correlate2d(red, mat, mode='full')
    y, x = np.unravel_index(np.argmax(corr), corr.shape)

    #print(corr[x,y])
    #print(x,y)
    #print(np.argmax(corr))

    fig, (ax_orig, ax_template, ax_corr, ax_corrPlot) = plt.subplots(4, 1, figsize=(15, 20))
    ax_orig.imshow(red, cmap='gray')
    ax_orig.set_title('Watermarked image')
    ax_orig.set_axis_off()
    ax_template.imshow(mat, cmap='gray')
    ax_template.set_title('Template')
    ax_template.set_axis_off()
    
    
    ax_corr.imshow(corr, cmap='gray')
    ax_corr.set_title('Cross-correlation')
    ax_corr.set_axis_off()
    ax_orig.plot(x, y, 'ro')
    
    ax_corrPlot.plot(np.absolute(corr))
    ax_corrPlot.set_title('Cross-correlation')
    #ax_corrPlot.set_axis_off()
    fig.show()
    fig.savefig('plot2.pdf')

    #corr[2,15]
    ind = np.unravel_index(np.argmax(corr, axis=None), corr.shape)
    i,j = ind
    print(ind)
    #print(corr(ind))


def compute_cross_correlation_watermark2(mat3d):
    #b, a = marca_position[12]
    mat = mat3d[35%11]
    img2 = cv2.imread(path+'/frame35.jpg')
    #print (type(img2))
    red = img2[:,:,2]
    red = (red - np.mean(red)) / (np.std(red) * len(red))
    mat = (mat - np.mean(mat)) / (np.std(mat))

    corr = signal.correlate2d(red, mat, mode='full')
    y, x = np.unravel_index(np.argmax(corr), corr.shape)

    #print(corr[x,y])
    #print(x,y)
    #print(np.argmax(corr))

    fig, (ax_orig, ax_template, ax_corr, ax_corrPlot) = plt.subplots(4, 1, figsize=(15, 20))
    ax_orig.imshow(red, cmap='gray')
    ax_orig.set_title('Watermarked image')
    ax_orig.set_axis_off()
    ax_template.imshow(mat, cmap='gray')
    ax_template.set_title('Template')
    ax_template.set_axis_off()
    
    
    ax_corr.imshow(corr, cmap='gray')
    ax_corr.set_title('Cross-correlation')
    ax_corr.set_axis_off()
    ax_orig.plot(x, y, 'ro')
    
    ax_corrPlot.plot(np.absolute(corr))
    ax_corrPlot.set_title('Cross-correlation')
    #ax_corrPlot.set_axis_off()
    fig.show()
    fig.savefig('plot3.pdf')

    #corr[2,15]
    ind = np.unravel_index(np.argmax(corr, axis=None), corr.shape)
    i,j = ind
    print(ind)
    #print(corr(ind))


def main():
    createFolders()
    convertVideo2Images()

    seq = [[4,6],[1,3],[3,2],[2,2],[5,6],[5,1],[2,5],[3,5],[1,4],[4,1]]
    mat3d = build3DL(mat, seq) 

    # print(mat)
    # print (seq)
    # print(mat3d)

    pathIn= path + "/"
    pathOut = path2
    insert_wm_images(pathIn, pathOut, mat3d)
    #orr = compute_cross_correlation(mat3d)
    #plot_cross_correlation3D(corr)

    pathIn= path2 + "/"
    pathOut = 'salida.mp4'
    fps = 15.0
    convert_frames_to_video(pathIn, pathOut, fps)
    convert_watermark_images()

    compute_cross_correlation_watermark(mat3d)
    compute_cross_correlation_watermark2(mat3d)

    
    

if __name__ == "__main__":
    main()
    
